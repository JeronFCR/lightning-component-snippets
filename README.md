
This extension contains code snippets for Lightning Components.

### Note

There will be an update adding JS, Apex and SLDS snippets. Actually, just 'CMP' snippets are available.

### Snippets

List of valid prefixes

| Trigger | Type | Example |
| -------- | -------- | -------- |
| l: | Lightning | l:pill, l:layoutItem, l:verticalNavigationSection... |
| a: | Aura | a:if, a:iteration, a:attribute... |
| i: | Interface | i:hasRecordId, i:appHostable, i:hasSObjectName... |
| h: | Handler | h:init |